/**
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict'

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

import path from 'path'
import gulp from 'gulp'
import del from 'del'
import runSequence from 'run-sequence'
import browserSync from 'browser-sync'
import gulpLoadPlugins from 'gulp-load-plugins'
import webkitAssign from 'webkit-assign/gulp'
import modRewrite from 'connect-modrewrite'

const $ = gulpLoadPlugins()
const reload = browserSync.reload

var serving = false

// Lint JavaScript
gulp.task('lint', function () {
  return gulp.src(['app/scripts/**/*.js', '!app/scripts/*.min.js'])
    .pipe($.standard())
    .pipe($.standard.reporter('default', {
      breakOnError: true,
      quiet: true
    }))
})

// Optimize images
gulp.task('images', () =>
  gulp.src(['app/images/**/*'])
    //.pipe($.cache($.imagemin({
    .pipe($.imagemin({
      progressive: true,
      interlaced: true
    })) // )
    .pipe(gulp.dest('dist/images'))
    .pipe($.size({title: 'images'}))
)

// Copy all files at the root level (app)
gulp.task('copy', () =>
  gulp.src([
    'app/*',
    '!app/*.html',
  ], {
    dot: true
  }).pipe(gulp.dest('dist'))
    .pipe($.size({title: 'copy'}))
)

// Copy translations
gulp.task('copy:i18n', () =>
  gulp.src([
    'app/i18n/**/*'
  ], {
    dot: true
  }).pipe(gulp.dest('dist/i18n'))
    .pipe($.size({title: 'copy:i18n'}))
)

// Copy fonts
gulp.task('copy:fonts', () =>
  gulp.src([
    'app/fonts/**/*',
    'node_modules/bootstrap/dist/fonts/*',
    'node_modules/font-awesome/fonts/*'
  ], {
    dot: true
  }).pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('dist/fonts'))
    .pipe($.size({title: 'copy:fonts'}))
)

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([
    'app/styles/**/*.scss',
    'app/styles/**/*.css'
  ])
    .pipe($.newer('.tmp/styles'))
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      precision: 10
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe($.if('*.css', $.cssnano({
      discardComments: {removeAll: true},
      autoprefixer: false,
      safe: true
    })))
    .pipe($.concat('main.css'))
    .pipe($.size({title: 'styles'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('dist/styles'))
})

gulp.task('styles:angularCSP', () => {
  return gulp.src([
    './node_modules/angular/angular-csp.css'
  ])
    .pipe($.cssnano())
    .pipe(gulp.dest('.tmp/'))
})

// Concatenate and minify JavaScript. Optionally transpiles ES2015 code to ES5.
// to enable ES2015 support remove the line `"only": "gulpfile.babel.js",` in the
// `.babelrc` file.
var scriptSources = [
  './app/scripts/app.js',
  './app/scripts/app.mock.js',
  './app/scripts/run.js',
  './app/scripts/run.mock.js',
  './app/scripts/*/**/*.js'
]

var scriptLibSources = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/angular/angular.min.js',
  './node_modules/angular-i18n/angular-locale_ca-es.js',
  './node_modules/angular-sanitize/angular-sanitize.min.js',
  './node_modules/angular-cookies/angular-cookies.min.js',
  './node_modules/@uirouter/angularjs/release/angular-ui-router.js',
  './node_modules/angular-mocks/angular-mocks.js',
  './node_modules/fastclick/lib/fastclick.js',
  './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
  './node_modules/angular-input-masks/releases/angular-input-masks-standalone.min.js'
]

gulp.task('scripts', () =>
  gulp.src(scriptSources)
    .pipe($.newer('.tmp/scripts'))
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe($.concat('main.js'))
    //.pipe($.ignore.exclude([ "**/*.map" ]))
    .pipe($.ngAnnotate())
    //.pipe($.uglify())
    /*  .on('error', function(e){
      console.log(e);
    }))*/
    // Output files
    .pipe($.size({title: 'scripts'}))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/scripts'))
)

function filesToInject (sources) {
  return serving === 'dist' ? [] : sources
}

gulp.task('inject', ['styles:angularCSP'], function () {
  return gulp.src('./app/index.html')
    .pipe($.inject(gulp.src(filesToInject(scriptSources), {read: false}), {
      relative: true,
      name: 'main'
    }))
    .pipe($.inject(gulp.src(filesToInject(scriptLibSources), {read: false}), {
      relative: true,
      name: 'libs'
    }))
    .pipe($.inject(gulp.src(['./.tmp/angular-csp.css']), {
      starttag: '<!-- inject:css:angular -->',
      transform: function (filePath, file) {
        // return file contents as string
        return '<style>' + file.contents.toString('utf8') + '</style>'
      }
    }))
    .pipe(gulp.dest('./.tmp'))
})

// Scan your HTML for assets & optimize them
gulp.task('html', ['inject'], () => {
  return gulp.src(['./.tmp/index.html', 'app/**/*.html', '!app/index.html'])
    .pipe($.useref({
      searchPath: '{.tmp,app}',
      noAssets: true
    }))

    // Minify any HTML
    .pipe($.if('*.html', $.htmlmin({
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeRedundantAttributes: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      removeOptionalTags: true
    })))
    // Output files
    .pipe($.if('*.html', $.size({title: 'html', showFiles: true})))
    .pipe(gulp.dest('dist'))
})

// Clean output directory
gulp.task('clean', () => del(['.tmp', 'dist/*', '!dist/.git'], {dot: true}))

gulp.task('setServingDev', function () {
  serving = 'dev'
})

gulp.task('setServingDist', function () {
  serving = 'dist'
})

gulp.task('serve', ['setServingDev'], cb =>
  runSequence(
    'serve:dev',
    cb
  )
)

gulp.task('serve:dist', ['setServingDist'], cb =>
  runSequence(
    'serve:dist:serving',
    cb
  )
)

var apiLocationRule = '^/pintiprint/(.*)$ http://localhost:37830/pintiprint/$1 [QSA,P,L]'
gulp.task('serve:dev', ['scripts', 'styles', 'copy:fonts', 'concat:libs', 'concat:css', 'inject'], () => {
  browserSync({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'WSK',
    // Allow scroll syncing across breakpoints
    scrollElementMapping: ['main', 'sidebar'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/node_modules': 'node_modules'
      }
    },
    port: 3000,
    middleware: [
      modRewrite([
        apiLocationRule
      ])
    ]
  })

  gulp.watch(['app/**/*.html'], reload)
  gulp.watch(['app/styles/**/*.{scss,css}'], ['styles', reload])
  gulp.watch(['app/scripts/**/*.js'], ['lint', 'scripts', reload])
  gulp.watch(['app/images/**/*'], reload)
  gulp.watch(['app/fonts/**/*'], reload)
})

// Build and serve the output from the dist build
gulp.task('serve:dist:serving', ['default'], () =>
  browserSync({
    notify: false,
    logPrefix: 'WSK',
    // Allow scroll syncing across breakpoints
    scrollElementMapping: ['main', 'sidebar', 'view', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: {
      baseDir: ['dist'],
    },
    port: 3001,
    middleware: [
      modRewrite([
        apiLocationRule
      ])
    ]
  })
)

// Build production files, the default task
gulp.task('default', ['clean'], cb =>
  runSequence(
    'styles',
    ['lint', 'html', 'scripts', 'images', 'copy', 'copy:i18n', 'copy:fonts', 'concat:libs', 'concat:css'],
    cb
  )
)

// Load custom tasks from the `tasks` directory
// Run: `npm install --save-dev require-dir` from the command-line
// try { require('require-dir')('tasks'); } catch (err) { console.error(err); }

// concat and copy js assets dependencies
gulp.task('concat:libs', function () {
  return gulp.src(scriptLibSources)
    .pipe($.if('angular.min.js', webkitAssign()))
    .pipe($.concat('libs.js'))
    .pipe($.size({title: 'scripts: libs'}))
    .pipe(gulp.dest('dist/scripts'))
})

// concat and copy css assets dependencies
gulp.task('concat:css', function () {
  return gulp.src([
    './node_modules/bootstrap/dist/css/bootstrap.min.css',
    './node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
    './node_modules/font-awesome/css/font-awesome.min.css',
    './node_modules/animate.css/animate.min.css'
  ])
    .pipe(gulp.dest('.tmp/styles'))
    .pipe($.concat('libs.css'))
    .pipe($.size({title: 'css: libs'}))
    .pipe(gulp.dest('dist/styles'))
})

gulp.task('clean:cache', function (done) {
  $.cache.clearAll()
  done()
})
