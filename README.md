# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install [Node.js](https://nodejs.org/en/)
* Dependencies: `$npm install`
* Local server: `$./node_modules/.bin/gulp serve`
* Deployment: `$./node_modules/.bin/gulp`

#### Enllaçar amb el projecte backend

Posem que ambdós projectes es troben dins la mateixa ubicació `/projectes/backend` i `/projectes/frontend`,
es tracta d'enllaçar el resultat del projecte frontend amb el src del projecte backend.

```
$ cd /projectes/backend/src/main/webapp/WEB-INF
$ ln -s /projectes/frontend/dist private
```

#### Redirigir crides a API des del Local server

Modificar l'arxiu `gulpfile.babel.js` per a modificar el destí de les crides al backend:

```
var apiLocationRule = '^/backend/(.*)$ http://localhost:37830/backend/$1 [QSA,P,L]'
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
