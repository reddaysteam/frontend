(function () {
  'use strict'

  angular.module('pintiprintE2E')
    .run(runFunction)

  function runFunction ($httpBackend, mockUserService, mockComandesService, API) {
    console.log('mockdata')

    $httpBackend.whenPOST(endpointToRegexp(API.ENDPOINTS.LOGIN)).respond(function (method, url, data, headers, params) {
      console.log('mock', url)
      var user = {
        cognoms: 'Mates Flaques',
        infoTargetaBanc: '2013895545623781120654',
        municipi: 'Barcelona',
        telefon: '935689878',
        carrer: 'C. Llarg, 23',
        nif: '12345678A',
        userrole: 'ROLE_ADMIN',
        provincia: 'Barcelona',
        nom: 'Pere',
        cp: '08080',
        email: 'pmates@abc.eu',
        pais: 'Catalunya'
      }

      var responseHeaders = {
        'x-auth-token': '12345678A:1513504547600:7e81cb9be0fe98eadc6fe5826eb4be5ab7b03a0e6a9f1dd134468aafa667d0c4'
      }

      return [200, user, responseHeaders]
    })

    $httpBackend.whenGET(endpointToRegexp(API.ENDPOINTS.COMANDES)).respond(function (method, url, data, headers, params) {
      console.log('mock', url)
      return [200, mockComandesService.readAll(), {}]
    })

    $httpBackend.whenGET(endpointToRegexp(API.ENDPOINTS.USUARIS)).respond(function (method, url, data, headers, params) {
      console.log('mock', url)
      return [200, mockUserService.readAll(), {}]
    })

    $httpBackend.whenGET(/.*/).passThrough()
    $httpBackend.whenHEAD(/.*/).passThrough()
    $httpBackend.whenDELETE(/.*/).passThrough()
    $httpBackend.whenPOST(/.*/).passThrough()
    $httpBackend.whenPUT(/.*/).passThrough()
    $httpBackend.whenPATCH(/.*/).passThrough()

    function endpointToRegexp (endpoint) {
      return new RegExp('\\' + endpoint + '$')
    }
  }
})()
