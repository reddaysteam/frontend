(function () {
  'use strict'

  angular
    .module('pintiprintE2E', [
      'pintiprint',
      'ngMockE2E'
    ])
})()
