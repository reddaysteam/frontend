(function () {
  'use strict'

  angular.module('pintiprint')
    .component('novaComanda', {
      controller: function (comandesService, $state) {
        var ctrl = this
        ctrl.fileUpload = comandesService.upload
        ctrl.formData = {}
        ctrl.formData.costUnitari = 15.00
        ctrl.files = null

        ctrl.updateTotal = function () {
          ctrl.formData.subTotal = ctrl.formData.quantitat * ctrl.formData.costUnitari
        }

        this.submit = function () {
          comandesService.create(ctrl.formData).then(function () {
            $state.go('private.comandes')
          })
        }
      },
      template: `
        <div class="col-md-4 col-md-offset-4 login">
          <form name="novaComanda" ng-submit="$ctrl.submit()" class="form-horizontal">
            <fieldset>
              <legend class="text-uppercase">Nova comanda</legend>

              <label for="quantitat"><span>Quantitat:</span></label>
              <div class="form-group">
                <input id="quantitat" type="number" min="1" class="form-control" required="required" 
                placeholder="Quantitat a imprimir" ng-model="$ctrl.formData.quantitat" 
                ng-change="$ctrl.updateTotal()"/>
              </div>

              <label for="costUnitari"><span>Cost Unitari:</span></label>
              <div class="form-group">
                <input id="costUnitari" type="text" class="form-control" placeholder="Cost Unitari" 
                ng-model="$ctrl.formData.costUnitari" ui-money-mask="2" ng-readonly="true"/>
              </div>

              <label for="costTotal"><span>Cost Total:</span></label>
              <div class="form-group">
                <input id="costTotal" type="text" class="form-control" placeholder="Cost Total" 
                ng-model="$ctrl.formData.subTotal" ui-money-mask="2" ng-readonly="true"/>
              </div>

              <div class="form-group">
                <input id="pathFitxer3d" type="hidden" class="form-control" required="required" 
                placeholder="ubicació" ng-model="$ctrl.formData.pathFitxer3d" hidden/>
              </div>
              <div class="form-group text-center">
                <input type="file" name="file" 
                onchange="angular.element(this).scope().$ctrl.fileUpload(this.files, angular.element(this).scope().$ctrl.formData)"/>
              </div>
              <div class="form-group text-center">
                <input type="submit" id="enter" class="btn btn-default" value="Entrar"/>
              </div>

            </fieldset>
          </form>
        </div>
      `
    })
})()
