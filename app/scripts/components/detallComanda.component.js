(function () {
  'use strict'

  angular.module('pintiprint')
    .component('comanda', {
      bindings: {
        comanda: '<'
      },
      controller: function (comandesService, COMANDA, constantsService, userService) {
        var ctrl = this
        ctrl.download = comandesService.download
        ctrl.isClient = userService.isClient
        ctrl.estats = COMANDA
        this.$onInit = function () {
          console.log('comanda', this.comanda)
        }

        ctrl.onStateChange = function () {
          if (ctrl.comanda.estat === constantsService.keyOf(COMANDA.ENVIADA, COMANDA)) {
            ctrl.comanda.dataEnviament = new Date()
          }
          comandesService.update(ctrl.comanda)
        }
      },
      template: `
        <div>
          <h3><span class="mayusculator">Detall de la comanda <strong>{{$ctrl.comanda.id}}</strong></span></h3>

          <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Quantitat</th>
                        <th scope="col">Fitxer</th>
                        <th scope="col">Preu</th>
                        <th scope="col">Estat</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td data-label="Data">
                          {{$ctrl.comanda.dataCreacio | date:'dd-MM-yyyy HH:mm:ss'}}
                        </td>
  
                        <td data-label="Quantitat">
                           {{$ctrl.comanda.quantitat}}
                        </td>
  
                        <td data-label="Fitxer">
                            <a ng-click="$ctrl.download($ctrl.comanda.id)">{{$ctrl.comanda.pathFitxer3d}}</a>
                        </td>
                        
                        <td data-label="Preu">
                          {{$ctrl.comanda.subTotal}}
                        </td>
  
                        <td ng-if="$ctrl.isClient()" data-label="Estat">
                            {{$ctrl.comanda.estat}}
                        </td>
  
                        <td ng-if="!$ctrl.isClient()" data-label="Estat">
                            <select ng-model="$ctrl.comanda.estat"
                                    ng-options="key as value for (key, value) in $ctrl.estats"
                                    ng-change="$ctrl.onStateChange()">
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
      `
    })
})()
