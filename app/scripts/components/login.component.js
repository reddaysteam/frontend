(function () {
  'use strict'

  angular.module('pintiprint')
    .component('login', {
      controller: function ($http, API, $state, sessionService, userService) {
        this.formData = {}
        this.errorMessage = ''
        var _this = this

        this.submit = function () {
          resetErrorMessage()

          $http({
            method: 'POST',
            url: API.BASE + API.ENDPOINTS.LOGIN,
            data: this.formData
          }).then(function (response) {
            console.log(response.data)
            sessionService.open(response.headers('X-Auth-Token'))
            userService.set(response.data)
            $state.go('private.comandes')
          }, function (response) {
            _this.errorMessage = response.statusText
          })
        }

        this.inputChange = function () {
          resetErrorMessage()
        }

        function resetErrorMessage () {
          if (_this.errorMessage !== '') {
            _this.errorMessage = ''
          }
        }
      },
      template: `
        <div class="col-md-4 col-md-offset-4 login">
          <form name="login" ng-submit="$ctrl.submit()" class="form-horizontal">
            <fieldset>
              <legend class="text-uppercase">Iniciar sessió</legend>
              <div class="form-group">
                <input id="nif" type="nif" class="form-control" required="required" placeholder="NIF" ng-model="$ctrl.formData.nif" ng-change="$ctrl.inputChange()"/>
              </div>
              <div class="form-group">
                <input id="contrasenya" type="password" class="form-control" required="required" placeholder="contrassenya" ng-model="$ctrl.formData.contrasenya" ng-change="$ctrl.inputChange()"/>
              </div>
              <div class="form-group text-center">
                <input type="submit" id="enter" class="btn btn-default" value ="Entrar"/>
              </div>
            </fieldset>
          </form>
          <div ng-bind="$ctrl.errorMessage"></div>
          <p class="text-center">Vols registrar-te?</p>
          <p class="text-center"><a href="../signup"><strong>Registra't</strong></a> amb un simple formulari</p>
        </div>
      `
    })
})()
