(function () {
  'use strict'

  angular.module('pintiprint')
    .component('comandes', {
      bindings: {
        comandes: '<'
      },
      controller: function (COMANDA) {
        var ctrl = this
        ctrl.estats = COMANDA
        this.$onInit = function () {
          console.log('comandes', this.comandes)
        }
      },
      // styleUrls: ['styles/llistacomandes.css'],
      template: `
        <div>
          <h3><span class="mayusculator">Llistat de comandes</span></h3>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col"># Codi</th>
                  <th scope="col">Data de creació</th>
                  <th scope="col">Estat</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="comanda in $ctrl.comandes">
                  <td data-label="Codi Comanda">
                    <a ui-sref="private.comanda({ comandaId: comanda.id, data: comanda })"> {{comanda.id}}</a>
                  </td>
                  <td data-label="Data de creació">
                    {{comanda.dataCreacio | date:'dd-MM-yyyy HH:mm:ss'}}
                  </td>
                  <td data-label="Estat">
                    {{$ctrl.estats[comanda.estat]}}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      `
    })
})()
