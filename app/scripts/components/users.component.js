(function () {
  'use strict'

  angular.module('pintiprint')
    .component('users', {
      bindings: {
        // one-way input binding, e.g.,
        // <users users="$parentCtrl.userlist"></users>
        // automatically bound to `users` on the controller
        users: '<'
      },
      controller: function (userService, $state, ROLES) {
        var ctrl = this
        ctrl.roles = ROLES
        ctrl.currentUser = userService.get().nif
        ctrl.deleteUser = function (nif) {
          userService.del(nif).then(function () {
            $state.go('private.users', null, { reload: true })
          })
        }

        this.$onInit = function () {
          console.log('users', this.users)
        }
      },
      template: `
        <div>
          <h2>Llista d'usuaris</h2>
          <div ng-repeat="role in $ctrl.roles">
            <h3>{{role | beautifyRole}}</h3>
            <ul class="userList">
              <li ng-repeat="user in $ctrl.users | filter:{nif: '!' + $ctrl.currentUser, role: role}">
                <div class="id">
                  <strong>{{user.nif}}</strong>  
                </div>
                <div class="name">
                  : {{user.nom}} {{user.cognoms}}  
                </div>
                <div class="trash">
                  <a ng-click="$ctrl.deleteUser(user.nif)"><i class="fa fa-trash" ></i></a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      `
    })
})()
