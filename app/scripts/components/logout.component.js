(function () {
  'use strict'

  angular.module('pintiprint')
    .component('logout', {
      controller: function (sessionService, API, $http) {
        console.log(sessionService.getToken())
        $http({
          method: 'GET',
          url: API.BASE + API.ENDPOINTS.LOGOUT,
          data: this.formData
        }).then(function (response) {
          console.log(response)
          sessionService.close()
          redirect()
        }, function (response) {
          sessionService.close()
          redirect()
        })

        function redirect () {
          window.setTimeout(function () {
            window.location = '/pintiprint'
          }, 3000)
        }
      },
      template: `
        <div class="col-md-4 col-md-offset-4 login">
          Logout amb èxit
        </div>
      `
    })
})()
