(function () {
  'use strict'

  angular.module('pintiprint')
    .component('private', {
      controller: function (sessionService, $state, userService) {
        this.isClient = userService.isClient
        this.isAdmin = userService.isAdmin
      },
      template: `
        <div id="content" class="row">
            <!-- LATERAL -->
            <div class="col-md-9 clearfix">
                <div ui-view></div>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="mayusculator">Àrea Privada</h3>
                    </div>

                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li ng-if="$ctrl.isClient()" ui-sref-active="active">
                                <a ui-sref="private.novaComanda"><i class="fa fa-plus"></i> Nova comanda</a>
                            </li>
                            <li ui-sref-active="active">
                                <a ui-sref="private.comandes"><i class="fa fa-list"></i> Les Meves comandes</a>
                            </li>
                            <li ng-if="$ctrl.isAdmin()" ui-sref-active="active">
                                <a ui-sref="private.users"><i class="fa fa-users"></i> Usuaris</a>
                            </li>
                            <!--<li>
                                <a href="#"><i class="fa fa-list-alt"></i>Antigues comandes</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-user"></i> El meu compte</a>
                            </li>-->
                            <li>
                                <a ui-sref="logout"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
      `
    })
})()
