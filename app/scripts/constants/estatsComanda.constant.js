(function () {
  'use strict'

  angular.module('pintiprint')
    .constant('COMANDA', {
      PENDENT: 'Pendent',
      ENPROCES: 'En procés',
      ENVIADA: 'Enviada',
      ENTREGADA: 'Entregada',
      CANCELLADA: 'Cancel·lada',
      DENEGADA: 'Denegada'
    })
})()
