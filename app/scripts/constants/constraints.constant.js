(function () {
  'use strict'

  angular.module('pintiprint')
    .constant('CONSTRAINTS', {
      TABLET: {
        MINHEIGHT: 600,
        MINWIDTH: 600
      }
    })
})()
