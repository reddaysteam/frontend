(function () {
  'use strict'

  angular.module('pintiprint')
    .constant('API', {
      BASE: '/pintiprint/api',
      ENDPOINTS: {
        LOGIN: '/login',
        LOGOUT: '/logout',
        COMANDES: '/comanda',
        USUARIS: '/usuaris',
        FILEUPLOAD: '/fileupload',
        FILEDOWNLOAD: '/file'
      }
    })
})()
