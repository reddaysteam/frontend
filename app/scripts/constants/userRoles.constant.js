(function () {
  'use strict'

  angular.module('pintiprint')
    .constant('ROLES', {
      ADMIN: 'ROLE_ADMIN',
      OPERARI: 'ROLE_OPERARI',
      CLIENT: 'ROLE_CLIENT'
    })
})()
