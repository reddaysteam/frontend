(function () {
  'use strict'

  angular.module('pintiprint')
    .constant('ROLENAMES', {
      ROLE_ADMIN: 'Administrador',
      ROLE_OPERARI: 'Operari',
      ROLE_CLIENT: 'Client'
    })
})()
