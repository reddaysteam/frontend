(function () {
  'use strict'

  angular.module('pintiprint', ['ui.router', 'ngCookies', 'ui.bootstrap', 'ui.utils.masks'])
    .config(function ($stateProvider, $httpProvider) {
      var states = [
        {
          name: 'login',
          url: '',
          component: 'login'
        },
        {
          name: 'logout',
          url: '',
          component: 'logout'
        },
        {
          name: 'private',
          url: '',
          abstract: true,
          component: 'private'
        },
        {
          name: 'private.comandes',
          url: '/comandes',
          component: 'comandes',
          resolve: {
            comandes: function (comandesService) {
              return comandesService.readAll()
            }
          }
        },
        {
          name: 'private.comanda',
          url: '/comandes/{comandaId}',
          component: 'comanda',
          params: {
            data: null
          },
          resolve: {
            comanda: function (comandesService, $transition$) {
              return $transition$.params().data ? $transition$.params().data : comandesService.read($transition$.params().comandaId)
            }
          }
        },
        {
          name: 'private.novaComanda',
          url: '/comandes/nova',
          component: 'novaComanda'
        },
        {
          name: 'private.users',
          url: '/users',
          component: 'users',
          resolve: {
            users: function (userService) {
              return userService.readAll()
            }
          }
        }
      ]

      angular.forEach(states, function (state) {
        $stateProvider.state(state)
      })

      $httpProvider.interceptors.push('authInterceptor')
    })
})()
