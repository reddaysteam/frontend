(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('authInterceptor', authInterceptor)

  function authInterceptor (sessionService, API) {
    return {
      'request': function (config) {
        if (config.url !== (API.BASE + API.ENDPOINTS.LOGIN)) {
          console.log('authInterceptor', sessionService.getToken(), config)
          config.headers = config.headers || {}
          config.headers['X-Auth-Token'] = sessionService.getToken()
        }
        return config
      }
    }
  }
})()
