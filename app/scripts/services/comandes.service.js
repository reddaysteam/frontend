/* globals FormData, Blob */
(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('comandesService', comandesService)

  function comandesService ($http, API) {
    return {
      create: create,
      read: read,
      readAll: readAll,
      update: update,
      del: del,
      upload: upload,
      download: download
    }

    function create (comanda) {
      return $http({
        method: 'POST',
        url: API.BASE + API.ENDPOINTS.COMANDES,
        data: comanda
      }).then(function (response) {
        console.log(response)
      })
    }

    function read (id) {
      return $http({
        method: 'GET',
        url: API.BASE + API.ENDPOINTS.COMANDES + '/' + id
      }).then(function (response) {
        console.log(response)
        return response.data
      })
    }

    function readAll () {
      return $http({
        method: 'GET',
        url: API.BASE + API.ENDPOINTS.COMANDES
      }).then(function (response) {
        console.log('comandesService.readAll', response)
        return response.data
      })
    }

    function update (comanda) {
      return $http({
        method: 'PUT',
        url: API.BASE + API.ENDPOINTS.COMANDES,
        data: comanda
      }).then(function (response) {
        console.log(response)
      })
    }

    function del () {

    }

    function upload (files, model) {
      console.log('upload', files)

      var fd = new FormData()
      // Take the first selected file
      fd.append('file', files[0])
      console.log(fd)

      return $http.post(
        API.BASE + API.ENDPOINTS.COMANDES + API.ENDPOINTS.FILEUPLOAD,
        fd,
        {
          // withCredentials: true,
          headers: {'Content-Type': undefined},
          transformRequest: angular.identity
        }
      ).then(function (response) {
        console.log('uploadsuccess', response)
        model.pathFitxer3d = response.data.uri
      }, function (response) {
        console.log('uploadKO', response)
      })
    }

    function download (id) {
      console.log(id)
      var url = API.BASE + API.ENDPOINTS.COMANDES + '/' + id + API.ENDPOINTS.FILEDOWNLOAD
      console.log(url)
      $http.get(url, {responseType: 'arraybuffer'})
        .then(function (response) {
          console.log(response.headers())
          processArrayBufferToBlob(response.data, response.headers)
        }, function (response) {
          console.log('download error')
        })
    }

    function processArrayBufferToBlob (data, headers) {
      var octetStreamMime = 'application/octet-stream'
      var success = false
      var blob = null
      var url = null

      // Get the headers
      headers = headers()

      // Get the filename from the x-filename header or default to "download.bin"
      var filename = headers['x-filename'] || 'download.zip'

      // Determine the content type from the header or default to "application/octet-stream"
      var contentType = headers['content-type'] || octetStreamMime

      try {
        // Try using msSaveBlob if supported
        blob = new Blob([data], {type: contentType})
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(blob, filename)
        } else {
          // Try using other saveBlob implementations, if available
          var saveBlob = navigator.webkitSaveBlob || navigator.mozSaveBlob || navigator.saveBlob
          if (saveBlob === undefined) {
            throw new DownloadException('Not supported')
          }
          saveBlob(blob, filename)
        }
        success = true
      } catch (ex) {
        console.log('saveBlob method failed with the following exception:')
        console.log(ex)
      }

      if (!success) {
        // Get the blob url creator
        var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL
        if (urlCreator) {
          // Try to use a download link
          var link = document.createElement('a')
          if ('download' in link) {
            // Try to simulate a click
            try {
              // Prepare a blob URL
              blob = new Blob([data], {type: contentType})
              url = urlCreator.createObjectURL(blob)
              link.setAttribute('href', url)

              // Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
              link.setAttribute('download', filename)

              // Simulate clicking the download link
              var event = document.createEvent('MouseEvents')
              event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null)
              link.dispatchEvent(event)
              success = true
            } catch (ex) {
              console.log('Download link method with simulated click failed with the following exception:')
              console.log(ex)
            }
          }

          if (!success) {
            // Fallback to window.location method
            try {
              // Prepare a blob URL
              // Use application/octet-stream when using window.location to force download
              blob = new Blob([data], {type: octetStreamMime})
              url = urlCreator.createObjectURL(blob)
              window.location = url
              success = true
            } catch (ex) {
              console.log('Download link method with window.location failed with the following exception:')
              console.log(ex)
            }
          }
        }
      }

      if (!success) {
        // Fallback to window.open method
        console.log('No methods worked for saving the arraybuffer, using last resort window.open')
        window.open(url, '_blank', '')
      }
    }

    function DownloadException (message) {
      this.message = message
      this.name = 'DownloadException'
    }
  }
})()
