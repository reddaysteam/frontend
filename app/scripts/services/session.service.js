(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('sessionService', sessionService)

  function sessionService () {
    var status = false
    var token = null

    return {
      open: open,
      close: close,
      isValid: isValid,
      getToken: getToken
    }

    function open (newToken) {
      status = true
      token = newToken
    }

    function close () {
      status = false
      token = null
    }

    function isValid () {
      return status
    }

    function getToken () {
      return token
    }
  }
})()
