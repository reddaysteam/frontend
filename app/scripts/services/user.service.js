(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('userService', userService)

  function userService (ROLES, API, $http) {
    var data = null

    return {
      set: set,
      get: get,
      isClient: isClient,
      isOperari: isOperari,
      isAdmin: isAdmin,
      create: create,
      read: read,
      readAll: readAll,
      update: update,
      del: del
    }

    function set (newData) {
      data = newData
    }

    function get () {
      return data
    }

    function hasRole (role) {
      return data.userrole.indexOf(role) !== -1
    }

    function isClient () {
      return hasRole(ROLES.CLIENT)
    }

    function isOperari () {
      return hasRole(ROLES.OPERARI)
    }

    function isAdmin () {
      return hasRole(ROLES.ADMIN)
    }

    function create () {

    }

    function read (id) {
      return $http({
        method: 'GET',
        url: API.BASE + API.ENDPOINTS.USUARIS + '/' + id
      }).then(function (response) {
        console.log('usuarisService.read', response)
        return response.data
      })
    }

    function readAll () {
      return $http({
        method: 'GET',
        url: API.BASE + API.ENDPOINTS.USUARIS
      }).then(function (response) {
        console.log('usuarisService.readAll', response)
        return response.data
      })
    }

    function update () {

    }

    function del (id) {
      return $http({
        method: 'DELETE',
        url: API.BASE + API.ENDPOINTS.USUARIS + '/' + id
      }).then(function (response) {
        console.log('usuarisService.delete', response)
      })
    }
  }
})()
