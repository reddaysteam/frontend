(function () {
  'use strict'

  angular
    .module('pintiprintE2E')
    .service('mockUserService', mockUserService)

  function mockUserService () {
    return {
      create: create,
      read: read,
      readAll: readAll,
      update: update,
      del: del
    }

    function create () {
    }

    function read (id) {
    }

    function readAll () {
      return [{
        'nif': '56781234N',
        'nom': 'Operari1',
        'cognoms': 'operari operari',
        'contrasenya': '$2a$10$c.sWCM/H6b1qKUWrqz1RR.b3Bc.om3myRzpOZmMNXiFgNj.Vcrr1.',
        'id_impressora': 1,
        'role': 'ROLE_OPERARI'
      }, {
        'nif': '67892345N',
        'nom': 'Operari2',
        'cognoms': 'operari operari',
        'contrasenya': '$2a$10$Y2YWEwlQRGv0JGwVD1PKk.MNerZ/yoqxz..wuR0InMeNTkCIJabjW',
        'id_impressora': null,
        'role': 'ROLE_OPERARI'
      }, {
        'nif': '12345678N',
        'nom': 'Admin1',
        'cognoms': 'admin admin',
        'contrasenya': '$2a$10$o4zlZEeop0EOf/WYRhkkQufbpBxxjpyVztPC21ds1IeT3MYiXjAjW',
        'role': 'ROLE_ADMIN'
      }, {
        'nif': '32564781R',
        'nom': 'Maria',
        'cognoms': 'Llopis Bull',
        'contrasenya': '$2a$10$W7tEVyulFMFMTSqZ4Gnn0.zKQko9rUh3QbCl3jd1o7whisfjQsx4S',
        'email': 'mllopis@gob.es',
        'carrer': 'C. Lleig, 45',
        'cp': '50620',
        'municipi': 'Teruel',
        'provincia': 'Teruel',
        'pais': 'Espanya',
        'telefon': '978236587',
        'infoTargetaBanc': '2519895545623781120654',
        'role': 'ROLE_CLIENT'
      }, {
        'nif': '85236974U',
        'nom': 'Josep',
        'cognoms': 'Trueta Grau',
        'contrasenya': '$2a$10$JZabIFOfcM/EzPBYDpaQQOGlG5EahSNYdk/HJmpj0O.S7l8UVeKzS',
        'email': 'jtrueta@trus.cu',
        'carrer': 'Pl. del Centre, 3',
        'cp': '08080',
        'municipi': 'Barcelona',
        'provincia': 'Barcelona',
        'pais': 'Catalunya',
        'telefon': '632458945',
        'infoTargetaBanc': '2013895545623781120654',
        'role': 'ROLE_CLIENT'
      }, {
        'nif': '87564321Z',
        'nom': 'Joan',
        'cognoms': 'Roures Tremps',
        'contrasenya': '$2a$10$UxCyjEdkpc9n9rDfooKQf.240.MArxwo4OQaRT/E/Gc6uFiRfr5xe',
        'email': 'jroures@@xyz.com',
        'carrer': 'C. Curt, 12',
        'cp': '08080',
        'municipi': 'Salt',
        'provincia': 'Girona',
        'pais': 'Catalunya',
        'telefon': '666123564',
        'infoTargetaBanc': '2983895545623781120654',
        'role': 'ROLE_CLIENT'
      }, {
        'nif': '56123789F',
        'nom': 'Marta',
        'cognoms': 'Gomis Turner',
        'contrasenya': '$2a$10$qI2.jWVWG44I4UIv4ZKK4e2txe3pv3CdFY/skPvD6vY7KaMGkWQVK',
        'email': 'mgomis@hot.xxx',
        'carrer': 'C. Bonic, 67',
        'cp': '28004',
        'municipi': 'Madrid',
        'provincia': 'Madrid',
        'pais': 'Espanya',
        'telefon': '915689756',
        'infoTargetaBanc': '2017895545623781120654',
        'role': 'ROLE_CLIENT'
      }, {
        'nif': '23456789N',
        'nom': 'Admin2',
        'cognoms': 'admin admin',
        'contrasenya': '$2a$10$4DwgkfEiIaGNJuMVZvPiBO2ggtHZiAaKufqVzxBbZPC6Pt4nZ3K5O',
        'role': 'ROLE_ADMIN'
      }, {
        'nif': '12345678A',
        'nom': 'Pere',
        'cognoms': 'Mates Flaques',
        'contrasenya': '$2a$10$SaDQveXtujDa48O8mQUANe6/AK9u7S2gqlU9WI8n521dWSmTXhRZS',
        'email': 'pmates@abc.eu',
        'carrer': 'C. Llarg, 23',
        'cp': '08080',
        'municipi': 'Barcelona',
        'provincia': 'Barcelona',
        'pais': 'Catalunya',
        'telefon': '935689878',
        'infoTargetaBanc': '2013895545623781120654',
        'role': 'ROLE_CLIENT'
      }, {
        'nif': '15975356T',
        'nom': 'Pepa',
        'cognoms': 'Santxis Diaz',
        'contrasenya': '$2a$10$OkJnYN12de/wRGRNbjL21.U0UEpIoREsdhWlG8UJ8TduR5E69baNe',
        'email': 'psantxis@gencat.cal',
        'carrer': 'Avgda. Diagonal, 666',
        'cp': '08080',
        'municipi': 'Barcelona',
        'provincia': 'Barcelona',
        'pais': 'Catalunya',
        'telefon': '678521265',
        'infoTargetaBanc': '2783895545623781120654',
        'role': 'ROLE_CLIENT'
      }]
    }

    function update () {

    }

    function del (id) {
    }
  }
})()
