(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('dataService', dataService)

  function dataService ($http, API) {
    var current = {}

    return {
      current: current,
      load: load
    }

    function load (req) {
      return $http(req).then(function successCallback (resp) {
        current.data = resp
      })
    }
  }
})()
