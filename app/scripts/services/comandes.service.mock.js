(function () {
  'use strict'

  angular.module('pintiprintE2E')
    .factory('mockComandesService', mockComandesService)

  function mockComandesService ($http, API) {
    return {
      create: create,
      read: read,
      readAll: readAll,
      update: update,
      del: del,
      upload: upload,
      download: download
    }

    function create (comanda) {
    }

    function read (id) {
    }

    function readAll () {
      return []
    }

    function update (comanda) {
    }

    function del () {
    }

    function upload (files, model) {
    }

    function download (id) {
    }
  }
})()
