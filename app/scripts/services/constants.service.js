(function () {
  'use strict'

  angular.module('pintiprint')
    .factory('constantsService', constantsService)

  function constantsService () {
    return {
      keyOf: keyOf
    }

    function keyOf (value, constant) {
      var result1 = Object.keys(constant).filter(function (key) {
        return constant[key] === value
      })[0]

      return result1
    }
  }
})()
