(function () {
  'use strict'

  angular.module('pintiprint')
    .filter('beautifyRole', roleFilter)

  function roleFilter (ROLENAMES) {
    return function (input) {
      return ROLENAMES[input]
    }
  }
})()
