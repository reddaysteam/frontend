(function () {
  'use strict'

  angular.module('pintiprint')
    .filter('unsafeURL', ['$sce', function ($sce) {
      return $sce.trustAsResourceUrl
    }])
})()
