(function () {
  'use strict'

  angular.module('pintiprint')
    .run(runFunction)

  function runFunction ($trace, $transitions, sessionService) {
    $trace.enable('TRANSITION')

    $transitions.onBefore({to: 'private.**'}, function (transition) {
      const stateService = transition.router.stateService

      if (!sessionService.isValid()) {
        return stateService.target('login')
      }
    })
  }
})()
